.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\AddToLLScript.psc"
  .modifyTime 1646899794
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:AddToLLScript Quest 
    .userFlags 0
    .docString ""
    .autoState 
    .structTable
      .struct LeveledListDatum
        .variable ListToUpdate leveleditem 
          .userFlags 0
          .initialValue None
          .docString "the leveled list item is being added to"
        .endVariable
        .variable FormToAdd form 
          .userFlags 0
          .initialValue None
          .docString "LeveledList or BaseObject to add to list"
        .endVariable
        .variable Level int 
          .userFlags 0
          .initialValue None
          .docString "what level to make this item in the list"
        .endVariable
        .variable count int 
          .userFlags 0
          .initialValue 1
          .docString "How many of this item in the list"
        .endVariable
      .endStruct
    .endStructTable
    .variableTable
      .variable ::LeveledListData_var code:addtollscript#leveledlistdatum[] const
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property LeveledListData code:addtollscript#leveledlistdatum[] auto
	    .userFlags 32
	    .docString ""
	    .autoVar ::LeveledListData_var
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup 
        .userFlags 0
        .docString ""
        .property LeveledListData
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnInit 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
          .endParamTable
          .localTable
            .local ::temp0 int
            .local ::temp1 bool
            .local i int
            .local ::temp3 leveleditem
            .local ::temp4 string
            .local ::temp5 string
            .local ::temp6 string
            .local ::temp7 form
            .local ::temp8 string
            .local ::temp9 string
            .local ::temp10 scriptobject
            .local ::nonevar none
            .local ::temp11 int
            .local ::temp12 int
            .local LLD code:addtollscript#leveledlistdatum
          .endLocalTable
          .code
            ASSIGN i 0 ;@line 17
            label0:
            ARRAYLENGTH ::temp0 ::LeveledListData_var ;@line 18
            COMPARELT ::temp1 i ::temp0 ;@line 18
            JUMPF ::temp1 label1 ;@line 18
            ARRAYGETELEMENT LLD ::LeveledListData_var i ;@line 19
            CAST ::temp10 self ;@line 20
            STRUCTGET ::temp3 LLD ListToUpdate ;@line 20
            CAST ::temp4 ::temp3 ;@line 20
            STRCAT ::temp5 "Updating ListToUpdate: " ::temp4 ;@line 20
            STRCAT ::temp6 ::temp5 ", by adding FormToAdd: " ;@line 20
            STRUCTGET ::temp7 LLD FormToAdd ;@line 20
            CAST ::temp8 ::temp7 ;@line 20
            STRCAT ::temp9 ::temp6 ::temp8 ;@line 20
            CALLSTATIC debug TraceSelf ::nonevar ::temp10 "OnInit()" ::temp9 ;@line 20
            STRUCTGET ::temp3 LLD ListToUpdate ;@line 22
            STRUCTGET ::temp7 LLD FormToAdd ;@line 22
            STRUCTGET ::temp11 LLD Level ;@line 22
            STRUCTGET ::temp12 LLD Count ;@line 22
            CALLMETHOD AddForm ::temp3 ::nonevar ::temp7 ::temp11 ::temp12 ;@line 22
            IADD i i 1 ;@line 23

            JUMP label0
            label1:
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable