.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\ThermalScopeScript.psc"
  .modifyTime 1644985121
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:ThermalScopeScript ActiveMagicEffect 
    .userFlags 0
    .docString ""
    .autoState 
    .structTable
    .endStructTable
    .variableTable
      .variable ::ReconTargets_var refcollectionalias const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::ThermalMarkerTime_var float const
        .userFlags 0
        .initialValue 1.0
      .endVariable
      .variable ::ThermalFXS_var effectshader const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable TargetRef actor 
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property ReconTargets refcollectionalias auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ReconTargets_var
	  .endProperty
	  .property ThermalMarkerTime float auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ThermalMarkerTime_var
	  .endProperty
	  .property ThermalFXS effectshader auto
	    .userFlags 0
	    .docString ""
	    .autoVar ::ThermalFXS_var
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup 
        .userFlags 0
        .docString ""
        .property ReconTargets
        .property ThermalMarkerTime
        .property ThermalFXS
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnEffectStart 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akTarget actor
            .param akCastor actor
          .endParamTable
          .localTable
            .local ::temp0 objectreference
            .local ::nonevar none
          .endLocalTable
          .code
            ASSIGN TargetRef akTarget ;@line 10

            CAST ::temp0 TargetRef ;@line 11
            CALLMETHOD AddRef ::ReconTargets_var ::nonevar ::temp0 ;@line 11
            CAST ::temp0 TargetRef ;@line 13
            CALLMETHOD Play ::ThermalFXS_var ::nonevar ::temp0 ::ThermalMarkerTime_var ;@line 13
          .endCode
        .endFunction
        .function OnEffectFinish 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
            .param akTarget actor
            .param akCaster actor
          .endParamTable
          .localTable
          .endLocalTable
          .code
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable