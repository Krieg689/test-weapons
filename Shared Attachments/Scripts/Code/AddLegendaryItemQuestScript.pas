.info
  .source "F:\\Mod Stuff\\Fallout 4\\MO2\\mods\\Code Shared Attachments\\Scripts\\Source\\User\\Code\\AddLegendaryItemQuestScript.psc"
  .modifyTime 1646899696
  .compileTime 1651164048
  .user "Sam"
  .computer "SAMS_DESKTOP"
.endInfo
.userFlagsRef
  .flag default 2
  .flag mandatory 5
  .flag collapsedonbase 4
  .flag hidden 0
  .flag collapsedonref 3
  .flag conditional 1
.endUserFlagsRef
.objectTable
  .object Code:AddLegendaryItemQuestScript Quest 
    .userFlags 0
    .docString "Adds items to the main LegendaryItemQuest's script's array"
    .autoState 
    .structTable
    .endStructTable
    .variableTable
      .variable ::LegendaryItemQuest_var legendaryitemquestscript const
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::LegendaryModRules_var legendaryitemquestscript#legendarymodrule[] 
        .userFlags 0
        .initialValue None
      .endVariable
      .variable ::AmmoData_var legendaryitemquestscript#ammodatum[] const
        .userFlags 0
        .initialValue None
      .endVariable
    .endVariableTable
    .propertyTable
	  .property LegendaryItemQuest legendaryitemquestscript auto
	    .userFlags 32
	    .docString "Autofill"
	    .autoVar ::LegendaryItemQuest_var
	  .endProperty
	  .property LegendaryModRules legendaryitemquestscript#legendarymodrule[] auto
	    .userFlags 0
	    .docString "After the weapon is spawned, check these rules, then add an appropriate legendary mod"
	    .autoVar ::LegendaryModRules_var
	  .endProperty
	  .property AmmoData legendaryitemquestscript#ammodatum[] auto
	    .userFlags 0
	    .docString "Used to add the correct leveled list of ammo that the spawned item uses"
	    .autoVar ::AmmoData_var
	  .endProperty
    .endPropertyTable
    .propertyGroupTable
      .propertyGroup 
        .userFlags 0
        .docString ""
        .property LegendaryItemQuest
        .property LegendaryModRules
        .property AmmoData
      .endPropertyGroup
    .endPropertyGroupTable
    .stateTable
      .state
        .function OnQuestInit 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
          .endParamTable
          .localTable
            .local ::nonevar none
          .endLocalTable
          .code
            CALLMETHOD AddItemsToBaseGameArray self ::nonevar  ;@line 14
          .endCode
        .endFunction
        .function AddItemsToBaseGameArray 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
          .endParamTable
          .localTable
            .local ::temp1 bool
            .local i int
            .local len int
            .local ::temp2 legendaryitemquestscript#legendarymodrule[]
            .local ::temp3 legendaryitemquestscript#legendarymodrule
            .local ::nonevar none
            .local ::temp5 legendaryitemquestscript#ammodatum[]
            .local ::temp6 legendaryitemquestscript#ammodatum
          .endLocalTable
          .code
            ASSIGN i 0 ;@line 20
            ARRAYLENGTH len ::LegendaryModRules_var ;@line 21
            label0:
            COMPARELT ::temp1 i len ;@line 24
            JUMPF ::temp1 label1 ;@line 24
            PROPGET LegendaryModRules ::LegendaryItemQuest_var ::temp2 ;@line 27
            ARRAYGETELEMENT ::temp3 ::LegendaryModRules_var i ;@line 27
            ARRAYADDELEMENTS ::temp2 ::temp3 1 ;@line 27
            IADD i i 1 ;@line 29

            JUMP label0
            label1:
            ASSIGN i 0 ;@line 33

            ARRAYLENGTH len ::AmmoData_var ;@line 34

            label2:
            COMPARELT ::temp1 i len ;@line 37
            JUMPF ::temp1 label3 ;@line 37
            PROPGET AmmoData ::LegendaryItemQuest_var ::temp5 ;@line 40
            ARRAYGETELEMENT ::temp6 ::AmmoData_var i ;@line 40
            ARRAYADDELEMENTS ::temp5 ::temp6 1 ;@line 40
            IADD i i 1 ;@line 42

            JUMP label2
            label3:
          .endCode
        .endFunction
        .function TraceArrays 
          .userFlags 0
          .docString ""
          .return NONE
          .paramTable
          .endParamTable
          .localTable
            .local ::temp7 string
            .local ::temp8 string
            .local ::nonevar none
            .local ::temp9 legendaryitemquestscript#legendarymodrule[]
            .local ::temp11 bool
            .local ::temp16 legendaryitemquestscript#ammodatum[]
            .local i int
            .local len int
            .local ::temp12 legendaryitemquestscript#legendarymodrule[]
            .local ::temp13 legendaryitemquestscript#legendarymodrule
            .local ::temp14 objectmod
            .local ::temp17 legendaryitemquestscript#ammodatum
            .local ::temp18 ammo
          .endLocalTable
          .code
            CAST ::temp7 self ;@line 49
            STRCAT ::temp8 ::temp7 "TraceArrays()" ;@line 49
            CALLSTATIC debug trace ::nonevar ::temp8 0 ;@line 49
            ASSIGN i 0 ;@line 51
            PROPGET LegendaryModRules ::LegendaryItemQuest_var ::temp9 ;@line 52
            ARRAYLENGTH len ::temp9 ;@line 52
            label4:
            COMPARELT ::temp11 i len ;@line 53
            JUMPF ::temp11 label5 ;@line 53
            PROPGET LegendaryModRules ::LegendaryItemQuest_var ::temp12 ;@line 55
            ARRAYGETELEMENT ::temp13 ::temp12 i ;@line 55
            STRUCTGET ::temp14 ::temp13 LegendaryObjectMod ;@line 55
            CAST ::temp7 ::temp14 ;@line 55
            CALLSTATIC debug trace ::nonevar ::temp7 0 ;@line 55
            IADD i i 1 ;@line 57

            JUMP label4
            label5:
            ASSIGN i 0 ;@line 60

            PROPGET AmmoData ::LegendaryItemQuest_var ::temp16 ;@line 61
            ARRAYLENGTH len ::temp16 ;@line 61

            label6:
            COMPARELT ::temp11 i len ;@line 62
            JUMPF ::temp11 label7 ;@line 62
            PROPGET AmmoData ::LegendaryItemQuest_var ::temp16 ;@line 64
            ARRAYGETELEMENT ::temp17 ::temp16 i ;@line 64
            STRUCTGET ::temp18 ::temp17 AmmoType ;@line 64
            CAST ::temp8 ::temp18 ;@line 64
            CALLSTATIC debug trace ::nonevar ::temp8 0 ;@line 64
            IADD i i 1 ;@line 66

            JUMP label6
            label7:
          .endCode
        .endFunction
      .endState
    .endStateTable
  .endObject
.endObjectTable