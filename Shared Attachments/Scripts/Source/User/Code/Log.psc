ScriptName Code:Log extends ScriptObject Const hidden

;-- Structs -----------------------------------------
Struct UserLog
	string Caller
	string FileName
EndStruct

Struct Point
	float X = 0.0
	float Y = 0.0
	float Z = 0.0
EndStruct


;-- Log Functions ---------------------------------------

UserLog Function GetLog(ScriptObject aScriptObject) Global DebugOnly
	UserLog contextLog = new UserLog
	contextLog.Caller = aScriptObject
	contextLog.FileName = GetTitle()
	return contextLog
EndFunction

Function WriteChangedValue(userlog UserLog, string propertyName, var fromValue, var toValue) global
	WriteLine(UserLog, ("Changing " + propertyName + " from " + fromValue as string + " to " + toValue as string) as var)
EndFunction

bool Function WriteLine(userlog UserLog, var text, int aiSeverity = 0) global
	string defaultFile = "Code_Log"
	If (UserLog == None)
		UserLog = new userlog
		UserLog.Caller = ""
		UserLog.FileName = defaultFile
	ElseIf (StringIsNoneOrEmpty(UserLog.FileName))
		UserLog.FileName = defaultFile
	EndIf
	text = (UserLog.Caller + " " + text as string) as var
	return Write(UserLog.FileName, text as string, aiSeverity)
EndFunction

Function WriteMessage(userlog UserLog, var text) global
	If (WriteLine(UserLog, text))
		Debug.MessageBox(text as string)
	EndIf
EndFunction

Function WriteNotification(userlog UserLog, var text) global
	If (WriteLine(UserLog, text))
		Debug.Notification(text as string)
	EndIf
EndFunction

bool Function Write(string FileName, string text, int aiSeverity = 0) global
	If (Debug.TraceUser(FileName, text, aiSeverity))
		return True
	Else
		Debug.OpenUserLog(FileName)
		return Debug.TraceUser(FileName, text, aiSeverity)
	EndIf
EndFunction

;-- Shared Functions ---------------------------------------

string Function GetTitle() Global
	return "Code Shared Attachments"
EndFunction

bool Function ChangeState(ScriptObject aScriptObject, string asNewState) global
	string stateName = aScriptObject.GetState()
	If (stateName != asNewState)
		aScriptObject.GoToState(asNewState)
		return True
	Else
		return False
	EndIf
EndFunction

bool Function StringIsNoneOrEmpty(string value) global
	return !value || value == ""
EndFunction

bool Function HasState(ScriptObject aScriptObject) global
	return aScriptObject.GetState() != ""
EndFunction

int Function BoolToInt(bool value) global
	If (value)
		return 1
	Else
		return 0
	EndIf
EndFunction

string Function PointToString(point value) global
	return "X:" + value.X as string + ", Y:" + value.Y as string + ", Z:" + value.Z as string
EndFunction