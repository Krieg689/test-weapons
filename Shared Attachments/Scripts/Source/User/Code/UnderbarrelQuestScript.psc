Scriptname Code:UnderBarrelQuestScript extends ReferenceAlias

import Code
import Code:Log

Group Properties
	GlobalVariable Property IsGrenadeLauncher Auto
	{Value for if the player has a grenade launcher. 0 = No, 1 = Yes}
	GlobalVariable Property IsMasterKey Auto
	{Value for if the player has a masterkey. 0 = No, 1 = Yes}
	GlobalVariable Property IsAttaching Auto
	{Value for if the script should attach the omod. 0 = Remove, 1 = Add}
	Keyword Property AnimsActive Auto
	Keyword Property AnimsGL Auto
	Keyword Property AnimsMK01 Auto
EndGroup

UserLog Log

Event OnAliasInit()
	Log = new UserLog
	Log.Caller = self
	Log.FileName = "Code_UnderBarrel"
EndEvent

Event OnItemEquipped(Form akBaseObject, ObjectReference akReference)
	Log = GetLog(self)
	Log.FileName = "Code_UnderBarrel"
	
	Actor Player = Game.GetPlayer()
	Form CurrentWeapon
	String CurrentWeaponName

	if Player.GetEquippedWeapon()
		CurrentWeapon = Player.GetEquippedWeapon()
		CurrentWeaponName = CurrentWeapon.GetName()
		WriteLine(Log, "Equiping "+CurrentWeapon+" AKA: "+CurrentWeaponName)
	endif
	
	if (Player.WornHasKeyword(AnimsGL))
        ;GL State
		IsGrenadeLauncher.SetValue(1.0)
        IsMasterKey.SetValue(0.0)
		if(Player.WornHasKeyword(AnimsActive))
			;Remove
            IsAttaching.SetValue(0.0)
		elseif(!Player.WornHasKeyword(AnimsActive))
            ;Add
			IsAttaching.SetValue(1.0)
        else
            ;Do Nothing
        endif
	elseif (Player.WornHasKeyword(AnimsMK01))
        ;MK01 State
		IsMasterKey.SetValue(1.0)
        IsGrenadeLauncher.SetValue(0.0)
		if(Player.WornHasKeyword(AnimsActive))
            ;Remove
			IsAttaching.SetValue(0.0)
		elseif(!Player.WornHasKeyword(AnimsActive))
            ;Add
			IsAttaching.SetValue(1.0)
        else
            ;Do Nothing
        endif
	else
        ;Do Nothing
    endif
EndEvent
