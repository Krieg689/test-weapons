Scriptname Code:ThermalAddSpellScript extends ActiveMagicEffect

Spell Property ThermalProjectileSpell Auto

Event OnEffectStart(Actor akTarget, Actor akCaster)
	;debug.trace(Self + "Start akTarget" + akTarget)
	akTarget.AddSpell(ThermalProjectileSpell)
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	;debug.trace(Self + "Finish akTarget" + akTarget)
	akTarget.RemoveSpell(ThermalProjectileSpell)
	;InterruptCast()
EndEvent