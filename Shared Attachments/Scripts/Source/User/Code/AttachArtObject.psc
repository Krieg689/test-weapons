Scriptname Code:AttachArtObject extends ActiveMagicEffect
{Attach VisualEffect to a named node}

VisualEffect Property AttachVE Auto Const
{attached art}
String Property myPlaceAtNode Auto Const
{node to place the art at}


Event OnEffectStart(Actor akTarget, Actor akCaster)
    AttachVE.Play(akCaster)
EndEvent

Event OnEffectFinish(Actor akTarget, Actor akCaster)
	akCaster.RemoveItem(CharmArmor, 1, True)
EndEvent