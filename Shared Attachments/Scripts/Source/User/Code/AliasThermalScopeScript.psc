Scriptname Code:AliasThermalScopeScript extends ReferenceAlias

import Code
import Code:Log

Group Properties
	Actor Property Player Hidden
		Actor Function Get()
			return Game.GetPlayer()
		EndFunction
	EndProperty
	Spell Property ThermalScopeSpell Auto
	Keyword Property HasScopeThermal Auto
	MagicEffect Property ThermalScopeEquippedEffect Auto
EndGroup

float ThermalTick = 0.25
int ThermalTimerId = 97097
UserLog Log

Event OnAliasInit()
	Log = new UserLog
	Log.Caller = self
	Log.FileName = "Code_ThermalScope"
EndEvent

Event OnItemEquipped(Form akBaseObject, ObjectReference akReference)
	Log = GetLog(self)
	Log.FileName = "Code_ThermalScope"
	
	Form CurrentWeapon
	String CurrentWeaponName
	if Player.GetEquippedWeapon()
		CurrentWeapon = Player.GetEquippedWeapon()
		CurrentWeaponName = CurrentWeapon.GetName()
		WriteLine(Log, "Equiping "+CurrentWeapon+" AKA: "+CurrentWeaponName)
	endif
	;check if the weapon has the keyword
	if (Player.HasMagicEffect(ThermalScopeEquippedEffect))
		WriteLine(Log, " Thermal Scope weapon detected, starting timer")
		;start the timer to check for ironsights
		StartTimer(ThermalTick, ThermalTimerId)
	endif
EndEvent

Event OnTimer(Int aiTimerID)
	WriteLine(Log, " aiTimerID: " + aiTimerID)

	;Check that the player is using a recon weapon to prevent timing mismatches
	if Player.HasMagicEffect(ThermalScopeEquippedEffect)
		if aiTimerID == ThermalTimerId && Player.IsInIronSights()
			;cast the spell if in ironsights
			WriteLine(Log, " casting Targeting spell")
			ThermalScopeSpell.RemoteCast(Player, Player)
		endif
		;start the timer again
		StartTimer(ThermalTick, ThermalTimerId)
		;cancel the spell
		;Player.InterruptCast()
	endif
EndEvent

Event OnItemUnequipped(Form akBaseObject, ObjectReference akReference)
	WriteLine(Log, " canceling Targeting timer")
	;cancel the timer if the weapon is unequipped
	CancelTimer(ThermalTimerId)
	;cancel the spell
	;Player.InterruptCast()
EndEvent